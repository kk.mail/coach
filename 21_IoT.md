# Table of Contents

## Introduction
* Embedded world
* Need of IoT
* Future scope
* Prerequist: Programming skills, interest to learn start-of-art

## Prerequist course
  * Programming language: Python, C, C++, Dart, JS
  * Python
  * Embedded C/C++
  * Dart / JS
  * Hardware interface: Serial port, I2C, Ethernet, SPI
  * Processors, memory management, 
  * Basic electronic 
  * USB based sensors interface

## Embedded [[1](https://manipal.edu/content/dam/manipal/mu/sois/documents/program-syllabus-oct-2020/ME%20(Automative%20Embedded%20System).pdf)]
  * Details of chapters: Ref: 1. page-14.
  * OS and RTOS
  * Windows and Linux
  * Process Management

## Sense-plan-act
* Sense:
  * Temperature
  * Infrared / Ultrasonic: Obstacles, Distance
  * Stress: Strain guage.
  * Photovoltic sensors
  * GPS
  * Camera (Image processing is another module)
* Plan
  * Center unit: Server (nodejs), capture, analysis, controller
  * Collect data
  * Analyze data (ML + AI: is another module)
  * Make sense of out it
* Act
  * What action to be taken
  * HW: 
  * Stepper motor
  * Relay
  * Hydraulic (option)
* Hardwares
  * Temperature
  * Humidity
  * Photovoltic /Light
  * Obstacles
  * Embedded board
  * Stepper motor
  * Relays
  
## Applications
  * Focus on Home Applications
    * Lights
    * Water
    * Humidity
    * Trespass

  * Autonomous (Self-driving)
    * Near by Obstacles
    * Object detection (Detail detectio is another module: Sensor fusion + Image processing)
    * Planning and decision is in another module: Robotics + Self-driving.

## Benefits
  * Coaching
  * Sensors + Hardware + Software
  * Certification
  * Further master course (At Germany)
  * Placement assistance
  * Possibilities of Europe Placement assistance.

## References
1. Automotive Embedded [[1](https://manipal.edu/content/dam/manipal/mu/sois/documents/program-syllabus-oct-2020/ME%20(Automative%20Embedded%20System).pdf)]
